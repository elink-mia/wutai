$(function() {
    $('.br-for').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.br-nav',
        adaptiveHeight: true
    });
    $('.br-nav').slick({
        dots: true,
        infinite: true,
        slidesToShow: 7,
        slidesToScroll: 1,
        arrows: true,
        focusOnSelect: true,
        dots: true,
        asNavFor: '.br-for',
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 5,
                    centerMode: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    centerMode: true
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 2,
                    centerMode: false
                }
            }
        ]
    });
    $('#twzipcode2').twzipcode();
});