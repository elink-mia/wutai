$(function() {
    var dec_height = function(first) {
        var content = $('#page').height(),
            win_h = $(window).height(),
            main_h = $('#main').height(),
            need_h = win_h - content;
        if (content < win_h || typeof first != 'undefined') {
            $('#main').css("min-height", main_h + need_h);
        }
    };
    dec_height(true);
    $(window).on('load', function() {
        footer_mb();
    });
    $(window).resize(function() {
        footer_mb();
        dec_height();
    });
    $('.br-for').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.br-nav',
        adaptiveHeight: true
    });
    $('.br-nav').slick({
        dots: true,
        infinite: true,
        slidesToShow: 7,
        slidesToScroll: 1,
        arrows: true,
        focusOnSelect: true,
        dots: true,
        asNavFor: '.br-for',
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 5,
                    centerMode: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    centerMode: true
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 2,
                    centerMode: false
                }
            }
        ]
    });
    $('.pros-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        asNavFor: '.pros-nav',
        fade: true
    });
    $('.pros-nav').slick({
        dots: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        asNavFor: '.pros-for',
        focusOnSelect: true
    });
    $(".pro-spec li").click(function() {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $('.pro-spec li.active').removeClass('active');
            $(this).addClass('active');
        }
    });
    $('.nav-tabs .nav-item').on('click', function() {
        dec_height();
    });
    //mob版 - 商品搜尋
    $("#mob-buy .btn").click(function(e) {
        e.preventDefault();
        $("#buy-select, #page").addClass("active");
        $('.overlay').fadeIn();
        $('body').addClass("no-scroll");
    });
    $("#buy-close, .overlay").click(function() {
        $("#buy-select, #page").removeClass("active");
        $('.overlay').fadeOut();
        $('body').removeClass("no-scroll");
    });
    $('#main .ellipsis').ellipsis({
        row: 2
    });
});
var footer_mb = function() {
    var mob_buyH = $('#mob-buy').innerHeight(),
        winW = $(window).width();
    if (winW < 992) {
        $('#main').css('margin-bottom', mob_buyH);
    }
};