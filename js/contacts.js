$(function() {
    $('.br-for').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.br-nav',
        adaptiveHeight: true
    });
    $('.br-nav').slick({
        dots: true,
        infinite: true,
        slidesToShow: 7,
        slidesToScroll: 1,
        arrows: true,
        focusOnSelect: true,
        dots: true,
        asNavFor: '.br-for',
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 5,
                    centerMode: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    centerMode: true
                }
            },
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 2,
                    centerMode: false
                }
            }
        ]
    });
    map_h();
    $(window).resize(function() {
        map_h();
    });
});
var map_h = function() {
    var map_h = $('.concept-body').outerHeight(),
        winW = $(window).width();
    if (winW >= 992) {
        $('#map').height(map_h);
    }
};