$(function() {
    fixed();
    $(window).on('load', function() {
        footer_mb();
    });
    $(window).resize(function() {
        footer_mb();
        fixed();
    });
    $('.SideBar, .icon-srh').on('click', function() {
        if ($('body').hasClass('no-scroll')) {
            $('body').removeClass("no-scroll");
        }
    });
    lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true
    });
    $('#twzipcode, #twzipcode2').twzipcode();
    $('#invoice-method').change(function() {
        $('.inv-').hide();
        $('.inv-' + $(this).val()).show("slow");
    }).change();
});
var footer_mb = function() {
    var mob_buyH = $('#mob-buy').innerHeight(),
        winW = $(window).width();
    if (winW < 992) {
        $('#main').css('margin-bottom', mob_buyH);
    }
};
var fixed = function() {
    var total_w = $('#deli-pay #total-block').width(),
        winH = $(window).height(),
        pageH = $('#page').height(),
        blankH = pageH - winH,
        pageH2 = $('#deli-pay .card').height(),
        headerH = $('#header').height();
        console.log(pageH, pageH2);
    $(window).scroll(function() {
        if ($(window).width() >= 992 && blankH > pageH2) {
            $('#deli-pay #total-block').addClass('fixed').width(total_w);
        }
        if ($(window).scrollTop() < headerH) {
            $('#deli-pay #total-block').removeClass('fixed');
        } 
    });
};